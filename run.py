from datetime import datetime

from helpers import get_user_data, connect_to_database, insert_user_data, io
from insert_reservation import get_reservation_data, insert_reservation_data
from send_email import send_email, sender_email, sender_password


def check_room_availability(room_number, date_to_reserve, reserve_time, until_date, until_time):
    if room_number < 6:
        conn, cursor = connect_to_database()
        cursor = conn.cursor()

        query3 = "SELECT * FROM reservation WHERE room_id IN (SELECT id FROM room WHERE room_number = ?)"
        cursor.execute(query3, (room_number,))

        all_reservations = cursor.fetchall()

        if len(all_reservations) > 0:
            query = "SELECT reservation_date, strftime('%H:%M', reservation_time), reservation_period_date, strftime('%H:%M', reservation_period_time), reserved_by_id FROM reservation r INNER JOIN user u ON r.reserved_by_id = u.id WHERE room_id = ?"
            cursor.execute(query, (room_number,))
            result = cursor.fetchone()

            get_user_data_query = "SELECT full_name FROM user WHERE id=?"
            cursor.execute(get_user_data_query, (result[4],))
            user_data = cursor.fetchone()

            if result:
                reservation_date = result[0]
                reservation_time = result[1]

                reservation_expires_date = result[2]
                reservation_expires_time = result[3]

                reservation_datetime = datetime.strptime(reservation_date + ' ' + reservation_time, '%Y-%m-%d %H:%M')
                reservation_expires_datetime = datetime.strptime(reservation_expires_date + ' ' + reservation_expires_time,
                                                                 '%Y-%m-%d %H:%M')
                user_entered_datetime = datetime.strptime(date_to_reserve + ' ' + reserve_time, '%Y-%m-%d %H:%M')

                if reservation_datetime <= user_entered_datetime <= reservation_expires_datetime:
                    print(f"Reservation already exists for the specified date and time. "
                          f"\n It was made by {user_data[0]}, \n Till {reservation_expires_date} {reservation_expires_time}")
                    return False
                else:
                    print('You can make a reservation on the date you entered.')
                    return True
            else:
                print(f'Room  number {room_number} is free, you can reserve it')
                return True
        else:
            return True
    else:
        print('Such room does not exists. Rooms are available only from 1 to 5')


def start():

    option = io()

    if option == 1:
        room_number, \
            reservation_date, \
            reservation_time, \
            reservation_period_date, \
            reservation_period_time = get_reservation_data()

        is_room_available = check_room_availability(room_number, reservation_date, reservation_time,
                                                    reservation_period_date, reservation_period_time)

        if is_room_available:
            print('Great! Room has no reservation on dates you entered')

        else:
            print('You can not make reservation on these dates. Reservation is already made someone else.')

    elif option == 2:
        room_number, \
            reservation_date, \
            reservation_time, \
            reservation_period_date, \
            reservation_period_time = get_reservation_data()

        is_room_available = check_room_availability(room_number, reservation_date, reservation_time, reservation_period_date, reservation_period_time)

        if is_room_available:
            print('You can make reservation. Enter your data below to make reservation:')

            name, email, phone = get_user_data()
            created_user_id = insert_user_data(name, email, phone)

            insert_reservation_data(room_number,
                                    created_user_id,
                                    reservation_date,
                                    reservation_time,
                                    reservation_period_date,
                                    reservation_period_time)

            subject = 'Your reservation was made successfully'
            message = f"""
                     You made reservation successfully. Your reservation details
                     Room number: {room_number} 
                     Reservation Date: {reservation_date}
                     Reservation Time: {reservation_time}
                     Your reservation will expiry in: {reservation_period_date} {reservation_period_time}
                     """
            send_email(sender_email, sender_password, email, subject, message)
        else:
            print('For dates you selected you can not make reservation')


start()
