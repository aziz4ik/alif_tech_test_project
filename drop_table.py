from helpers import connect_to_database


def clean_reservations_table():
    conn, cursor = connect_to_database()
    query = "DELETE FROM reservation"
    cursor.execute(query)
    conn.commit()
    conn.close()


# Call the function to clean the "reservations" table
clean_reservations_table()
