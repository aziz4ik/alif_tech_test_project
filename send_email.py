import smtplib
from email.mime.text import MIMEText


def send_email(sender_email, sender_password, recipient_email, subject, message):
    # Create a MIMEText object with the message content
    msg = MIMEText(message)

    # Set the sender, recipient, and subject fields
    msg['From'] = sender_email
    msg['To'] = recipient_email
    msg['Subject'] = subject

    try:
        # Create a SMTP connection to the email server
        smtp_server = smtplib.SMTP('smtp.gmail.com', 587)
        smtp_server.starttls()

        # Login to the email server
        smtp_server.login(sender_email, sender_password)

        # Send the email
        smtp_server.sendmail(sender_email, recipient_email, msg.as_string())

        # Close the connection to the email server
        smtp_server.quit()

        print("Email sent successfully!")
    except Exception as e:
        print("An error occurred while sending the email:", str(e))

# Example usage
sender_email = "iamgulyamov@gmail.com"
sender_password = "zxwtcwiekibcxeur"
recipient_email = "devazizbek07@gmail.com"
subject = "Hello from Python!"
message = "You successfully made reservation"
