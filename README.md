Hello **AlifTech** Team!

As part of the application process for the new job, I was given a test task to complete. I successfully completed the task and would like to share my approach and the outcome.

### Task Description

The test task involved creating command lines in python in order user
could check reservation or make reservation for the date and time user selected
and after that notify user via email or phone about his/her reservation details

*** 

### My Approach
To tackle the task, I followed a step-by-step approach, including
usage of SQLite DMS, python, sql skills.
I made sure to thoroughly understand the requirements and plan my solution accordingly.


***

### Outcome

After dedicating time and effort to complete the task, I am pleased to say that I successfully achieved the desired outcome.
Now it is possible to make reservation on date and time user entered and notify user about reservation via email
also check reservation 


***
This is completed project which you asked.
I wrote py script in order to generate 5 rooms in database as per requirements.
After running insert_room_data.py 5 rooms with numeration will be generated

Here I used latest python version 3.1.0 and SQLite as DMS and python built-in package mail.mime.text for sending email

***

***In order to run project:***
1. First open file create_tables.py and run it 
2. Then open file insert_room_data.py and run it (to create rooms in db starting from 1 to 5)
3. Execute run.py file 

You will see menu in order to make reservation or check reservation for your desired date and time