from helpers import connect_to_database, close_database_connection


def get_reservation_data():
    room_number = int(input('Enter room number: '))
    reservation_date = input('Reservation date (ENTER in format YYYY-MM-DD): ')
    reservation_time = input('Reservation time (ENTER in format (11:30): ')
    reservation_period_date = input('Reservation period date (format YYYY-MM-DD): ')
    reservation_period_time = input('Reservation period time (format 17:30): ')

    return room_number, \
        reservation_date, \
        reservation_time, \
        reservation_period_date, \
        reservation_period_time


def insert_reservation_data(room_number, user_id, reservation_date, reservation_time, reservation_period_date, reservation_period_time):
    conn, cursor = connect_to_database()
    query = "INSERT INTO reservation (room_id, reserved_by_id, reservation_date, reservation_time, " \
            "reservation_period_date, reservation_period_time) " \
            "VALUES (?, ?, ?, ?, ? ,?)"
    cursor.execute(query, (room_number, user_id, reservation_date, reservation_time, reservation_period_date, reservation_period_time))
    close_database_connection(conn)
