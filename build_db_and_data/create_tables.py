import sqlite3

conn = sqlite3.connect('../reservations.db')


conn.execute('''
    CREATE TABLE IF NOT EXISTS user (
        id INTEGER PRIMARY KEY,
        full_name TEXT,
        email TEXT,
        phone INTEGER
    )
''')

conn.execute('''
    CREATE TABLE IF NOT EXISTS room (
        id INTEGER PRIMARY KEY,
        room_number INTEGER
    )
''')

conn.execute('''
    CREATE TABLE IF NOT EXISTS reservation (
        id INTEGER PRIMARY KEY,
        room_id INTEGER,
        reserved_by_id INTEGER,
        reservation_date DATE,
        reservation_time TIME,
        reservation_period_date DATE,
        reservation_period_time TIME,
        FOREIGN KEY (room_id)
            REFERENCES room (id),
        FOREIGN KEY (reserved_by_id)
            REFERENCES user (id)

    )
''')

conn.commit()
conn.close()
