import sqlite3

from helpers import connect_to_database

conn, cursor = connect_to_database()


sql_statements = """
    INSERT INTO room (room_number) VALUES ('1');
    INSERT INTO room (room_number) VALUES ('2');
    INSERT INTO room (room_number) VALUES ('3');
    INSERT INTO room (room_number) VALUES ('4');
    INSERT INTO room (room_number) VALUES ('5');
"""

cursor.execute(sql_statements)

conn.commit()
conn.close()
