import sqlite3


def connect_to_database():
    conn = sqlite3.connect('reservations.db')
    cursor = conn.cursor()
    return conn, cursor


def close_database_connection(conn):
    conn.commit()
    conn.close()


def get_user_data():
    full_name = input('Enter your full name: ')
    email = input('Enter your email: ')
    phone = input('Enter your phone number: ')
    return full_name, email, phone


def insert_user_data(full_name, email, phone):
    conn, cursor = connect_to_database()
    query = "INSERT INTO user (full_name, email, phone) VALUES (?, ?, ?)"
    cursor.execute(query, (full_name, email, phone))
    user_id = cursor.lastrowid
    close_database_connection(conn)
    return user_id


def io():
    print(
        '''
        Select Option:
        1. Check room availability
        2. Reserve room
        '''
    )

    option = int(input('Enter option number: '))

    return option
